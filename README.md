# Craterzone Frontend Assignment



[![DEMO]](http://craterzone.getforge.io/)

#### DEMO URL : (http://craterzone.getforge.io/)

###### React js 16.12.0v
[![DEMO](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

# Features!

  > Search by name 
            - Rows can be filtered by name and will be shown as paginated rows
            
>User List is paginated (max 5 items per page ).

>All the columns are sortable.

>User Detail Page




### App Components

| Components | Details |
| ------ | ------ |
| App.js | Main components (Home Page) |
| DetailPage.js | Detailed Page Component |
| Pagination.js | Whole Pagination logic is inside this component |
| SearchBar.js | App header with search bar |


### Development

TO RUN THE CODE

Open your  Terminal and run these commands.


```sh
$ npm install
$ npm start
```

#### Building for Production
For production release:
```sh
$ npm run build
```

### Todos

 - Write MORE Tests
 - Page number and row clicked should be persisted after coming back from detailed page

License
----

MIT

