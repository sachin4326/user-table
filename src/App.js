import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { default as React, Component } from 'react';
import './App.css';
import SearchAppBar from './components/SearchAppBar';
import axios from 'axios';
import { apiUrl } from './config';
import Pagination from './components/Pagination';
import { userInfo } from 'os';
import ArrowDropDown from '@material-ui/icons/ArrowDropDown';
import ArrowDropUp from '@material-ui/icons/ArrowDropUp';
import Sort from '@material-ui/icons/Sort';
import { Link } from 'react-router-dom';


const ASC = 1;
const DESC = 0;

export default class App extends Component {



  constructor(props) {

    super();
    this.state = {
      tableHead: [{
        label: "Profile",
        sorting: false,
     
        field: "avatar"

      }, { label: "Name", sorting: true, sorted: null, field: "name" },
      { label: "Created At", sorting: true, sorted: null, field: "createdAt" }],
      sourceUserList: [],
      allUserList: [],
      offset:0,
      to:0,
      userList: [],
      perPage: 5,
      total: 0,
      searchVal: null,
      currentPage: null, totalPages: null
    }


  }
  getFormattedDate(date){

    return new Date(date).toDateString();
}



  handleOnChange = (e) => {

    this.setState({ [e.target.name]: e.target.value });
    this.filterData(e.target.value);

  }



  filter(list, filterValue, filterProp) {

    var results = [];



    list.forEach(function (item) {

      if (item[filterProp].toLowerCase().includes(filterValue.toLowerCase()))
        results.push(item);


    });
    return results;


  }

  filterData = (value) => {

    debugger;
    const { sourceUserList, allUserList } = this.state;

    let filterData = [];

    if (value != "") {
      filterData = this.filter(sourceUserList, value, "name");
    }
    else {

      filterData = sourceUserList;
    }


    filterData = this.sortBy(filterData,"id",ASC);

    this.setState({ allUserList: filterData });
    this.setState({ totalRecords: filterData.length });
    const totalPages = Math.ceil(filterData.length / this.state.perPage);
    this.onPageChanged(
      {currentPage : 1,
      totalPages:totalPages, 
      pageLimit:this.state.perPage}
    );



  }

sortBy(userList,property,sortas){
 return userList.sort(function (a, b) {
    var x = a[property].toLowerCase();
    var y = b[property].toLowerCase();
    if (x < y) { return sortas == 1 ? -1 : 1; }
    if (x > y) { return sortas == 1 ? 1 : -1; }
    return 0;
  });
}

  sortColumn = (i) => {
    let { userList } = this.state;
    let { tableHead } = this.state;

    if (tableHead[i].sorted != null) {
      tableHead[i].sorted = !tableHead[i].sorted;
    } else {
      tableHead[i].sorted = 1;
    }


    userList = this.sortBy(userList,tableHead[i].field,
      tableHead[i].sorted );




    this.setState({ userList, tableHead });

  }

  componentDidMount() {

    this.getUserDateFromApi();

  }



  getUserDateFromApi = async () => {

    const response = await axios.get(apiUrl);
    const allUserList = response.data;
   // const allUserList = response.data.splice(0, this.state.perPage);
    this.setState({ allUserList: allUserList });
    this.setState({ sourceUserList: allUserList });

    this.setState({ userList: allUserList });
    this.setState({ total: allUserList.length });

    const totalPages = Math.ceil(allUserList.length / this.state.perPage);
    this.onPageChanged(
      {currentPage : 1,
      totalPages:totalPages, 
      pageLimit:this.state.perPage}
    );

  }

goToDetailPage(id){

  this.props.history.push("/user/"+id);
}

  onPageChanged = data => {
    const { allUserList } = this.state;
    const { currentPage, totalPages, pageLimit } = data;

    const offset = (currentPage - 1) * pageLimit;
    const userList = allUserList.slice(offset, offset + pageLimit);

    this.setState({ offset: offset, to: (offset + pageLimit) });

    this.setState({ currentPage, userList, totalPages });
  }


  render() {
    const { userList, allUserList, searchVal, tableHead, offset, to } = this.state;
    const totalRecords = allUserList.length;
    return (
      <div className="dull-bg">
        <SearchAppBar hideSearch={false} handleOnChange={this.handleOnChange} searchVal={searchVal} />
        <Container maxWidth="lg"  >
          <Grid container>

            <Grid item xs={12} md={12} className="mt-5" lg={12}>
              <h2>{(offset+1) + '-' + (to<=totalRecords ? to : totalRecords)} of {totalRecords}</h2>
              <Paper>

                <table className="pure-table">
                  <thead>
                    <tr>
                      {tableHead.map((head, i) => {

                        return (
                          <th onClick={() => head.sorting ? this.sortColumn(i) : ''}>
                            {head.label}
                            {head.sorting ?
                              <span> {head.sorted == null ? <Sort /> : head.sorted == 1 ? <ArrowDropDown /> : <ArrowDropUp />
                              }
                              </span> : ''}</th>
                        )

                      })}




                    </tr>
                  </thead>
                  <tbody>

                    {userList.map((item, i) => {


                      return (<tr onClick={()=>this.goToDetailPage(item.id)} className={i % 2 == 0 ? "pure-table-odd" : "pure-table"}>

                        <td><img src={item.avatar} className="avatar" /></td>
                        <td>{item.name}</td>

                        <td>{this.getFormattedDate(item.createdAt)}</td>


                      </tr>)


                    })}


                  </tbody>
                </table>




              </Paper>
            </Grid>
            {totalRecords > 0 ?
              <Pagination totalRecords={totalRecords}
                pageLimit={3}
                pageNeighbours={1}
                onPageChanged={this.onPageChanged}
              /> : ''}


          </Grid>

        </Container>

      </div>
    );
  }
}


