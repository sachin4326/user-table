import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { default as React, Component } from 'react';
import './App.css';
import SearchAppBar from './components/SearchAppBar';
import axios from 'axios';
import { apiUrl } from './config';
import Pagination from './components/Pagination';
import { userInfo } from 'os';
import ArrowDropDown from '@material-ui/icons/ArrowDropDown';
import ArrowDropUp from '@material-ui/icons/ArrowDropUp';
import Sort from '@material-ui/icons/Sort';



export default class DetailPage extends Component {



  constructor(props) {

    super();
    this.state = {    
      userList: [],
      currentUser:{}
    }


  }





  filter(list, filterValue, filterProp) {

    var results = [];



    list.forEach(function (item) {

      if (item[filterProp]==filterValue)
        results.push(item);


    });
    return results;


  }

 

   

 
  componentDidMount() {
    const id = this.props.match.params.id;
    this.getUserDateFromApi(id);

  }



  getUserDateFromApi = async (id) => {

    const response = await axios.get(apiUrl);
    const allUserList = response.data;
   
  

    this.setState({ userList: allUserList });
   const currentUser = this.filter(allUserList,id,"id")[0];
   this.setState({currentUser});


  }


  getFormattedDate(date){
      if(date)return new Date(date).toDateString();
  }

  onBackPressed=()=>{
   
   this.props.history.goBack();
  }

  render() {
  const {currentUser} = this.state;
    return (
      <div className="dull-bg">
         <SearchAppBar onBackPressed={this.onBackPressed} hideSearch={true} />
        <Container maxWidth="lg" >
          <Grid container spacing={3} className="mt-5">

            <Grid item xs={12} md={12} lg={12}>
              <Paper>
    <div className="row profile-box">    <div className="col-md-3">

    <img className="profile" src={currentUser.avatar} />
    </div>
    <div className="col-md-9">

    <h1>{currentUser.name}</h1>
    <h4>Created At: {this.getFormattedDate(currentUser.createdAt)}</h4>
   <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
</div>
    </div>


                


              </Paper>
            </Grid>


          </Grid>

        </Container>

      </div>
    );
  }
}


