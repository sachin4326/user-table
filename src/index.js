import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import createHistory from "history/createBrowserHistory"
import App from './App';
import DetailPage from './DetailPage';

export const history = createHistory()

history.listen((location, action) => {
  window.scrollTo(0, 0)
})


ReactDOM.render(<Router history={history}>



<Route exact path="/user/:id" component={DetailPage} />
<Route exact  path="/" component={App} />

</Router>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
